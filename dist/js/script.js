const btn = document.querySelector('.burger-icon')
const menu = document.querySelector('.menu')

btn.addEventListener('click', () => {
    btn.classList.toggle('open')
    menu.classList.toggle('active')
})